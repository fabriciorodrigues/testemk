unit uModel.Produto;

interface
uses
    System.SysUtils;
    type
    TModelProduto = class
      private
        FPreco: Currency;
        FDescricao: String;
        FCodigo: Integer;
        procedure SetCodigo(const Value: Integer);
        procedure SetDescricao(const Value: String);
        procedure SetPreco(const Value: Currency);

      public
        property Codigo     :Integer read FCodigo write SetCodigo;
        property Descricao  : String read FDescricao write SetDescricao;
        property Preco      : Currency read FPreco write SetPreco;

    end;

implementation

{ TModelCliente }

procedure TModelProduto.SetCodigo(const Value: Integer);
begin
  FCodigo := Value;
end;

procedure TModelProduto.SetDescricao(const Value: String);
begin
  FDescricao := Value;
end;

procedure TModelProduto.SetPreco(const Value: Currency);
begin
  FPreco := Value;
end;

end.
