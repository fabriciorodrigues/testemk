unit uModel.Pedido.Itens;

interface
  uses
    System.SysUtils;
    type
    TModelPedidoItens = class
      private
      FVl_Total: Currency;
      FNum_Pedido: Integer;
      FQtd: Currency;
      FCod_Produto: Integer;
      FId_Ped_Itens: Integer;
      FVl_Unitario: Currency;
      procedure SetCod_Produto(const Value: Integer);
      procedure SetId_Ped_Itens(const Value: Integer);
      procedure SetNum_Pedido(const Value: Integer);
      procedure SetQtd(const Value: Currency);
      procedure SetVl_Total(const Value: Currency);
      procedure SetVl_Unitario(const Value: Currency);

      public
      property Id_Ped_Itens   :Integer read FId_Ped_Itens write SetId_Ped_Itens;
      property Num_Pedido     :Integer read FNum_Pedido write SetNum_Pedido;
      property Cod_Produto    :Integer read FCod_Produto write SetCod_Produto;
      property Qtd            :Currency read FQtd write SetQtd;
      property Vl_Unitario    :Currency read FVl_Unitario write SetVl_Unitario;
      property Vl_Total       :Currency read FVl_Total write SetVl_Total;
    end;

implementation

{ TModelPedidoItens }

procedure TModelPedidoItens.SetCod_Produto(const Value: Integer);
begin
  FCod_Produto := Value;
end;

procedure TModelPedidoItens.SetId_Ped_Itens(const Value: Integer);
begin
  FId_Ped_Itens := Value;
end;

procedure TModelPedidoItens.SetNum_Pedido(const Value: Integer);
begin
  FNum_Pedido := Value;
end;

procedure TModelPedidoItens.SetQtd(const Value: Currency);
begin
  FQtd := Value;
end;

procedure TModelPedidoItens.SetVl_Total(const Value: Currency);
begin
  FVl_Total := Value;
end;

procedure TModelPedidoItens.SetVl_Unitario(const Value: Currency);
begin
  FVl_Unitario := Value;
end;

end.
