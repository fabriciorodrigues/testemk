unit uModel.Pedido;

interface
  uses
    System.SysUtils;
    type
    TModelPedido = class
      private
        FNum_Pedido: Integer;
        FValor_Total: Currency;
        FData_Emissao: TDate;
        FCod_Cliente: Integer;
        FCodigo: Integer;
        procedure SetCod_Cliente(const Value: Integer);
        procedure SetData_Emissao(const Value: TDate);
        procedure SetNum_Pedido(const Value: Integer);
        procedure SetValor_Total(const Value: Currency);
        procedure SetCodigo(const Value: Integer);

      public
        property Num_Pedido   :Integer read FNum_Pedido write SetNum_Pedido;
        property Data_Emissao :TDate read FData_Emissao write SetData_Emissao;
        property Cod_Cliente  :Integer read FCod_Cliente write SetCod_Cliente;
        property Valor_Total  :Currency read FValor_Total write SetValor_Total;
        property Codigo: Integer read FCodigo write SetCodigo;
    end;

implementation

{ TModelProduto }

procedure TModelPedido.SetCodigo(const Value: Integer);
begin
  FCodigo := Value;
end;

procedure TModelPedido.SetCod_Cliente(const Value: Integer);
begin
  FCod_Cliente := Value;
end;

procedure TModelPedido.SetData_Emissao(const Value: TDate);
begin
  FData_Emissao := Value;
end;

procedure TModelPedido.SetNum_Pedido(const Value: Integer);
begin
  FNum_Pedido := Value;
end;

procedure TModelPedido.SetValor_Total(const Value: Currency);
begin
  FValor_Total := Value;
end;

end.
