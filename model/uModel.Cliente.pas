unit uModel.Cliente;

interface
  uses
    System.SysUtils;
    type
    TModelCliente = class
      private
        FUF: String;
        FCodigo: Integer;
        FNome: String;
        FCidade: String;
        procedure SetCidade(const Value: String);
        procedure SetCodigo(const Value: Integer);
        procedure SetNome(const Value: String);
        procedure SetUF(const Value: String);

      public
        property Codigo   :Integer read FCodigo write SetCodigo;
        property Nome     :String read FNome write SetNome;
        property Cidade   :String read FCidade write SetCidade;
        property UF       :String read FUF write SetUF;
    end;

implementation

{ TModelCliente }

procedure TModelCliente.SetCidade(const Value: String);
begin
  FCidade := Value;
end;

procedure TModelCliente.SetCodigo(const Value: Integer);
begin
  if Value <= 0 then
    FCodigo := Value
  else
    raise Exception.Create('C�digo cliente inv�lido!');
end;

procedure TModelCliente.SetNome(const Value: String);
begin
  FNome := Value;
end;

procedure TModelCliente.SetUF(const Value: String);
begin
  FUF := Value;
end;

end.
