unit uPedido_Vendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls;

type
  TfrmPedido = class(TForm)
    dsCliente: TDataSource;
    dsProduto: TDataSource;
    dsProductOrder: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtCodCliente: TEdit;
    edtNome: TEdit;
    edtCidade: TEdit;
    edtUF: TEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    label20: TLabel;
    Label8: TLabel;
    edtCodProd: TEdit;
    edtNomeProd: TEdit;
    edtQtd: TEdit;
    edtVlUnitario: TEdit;
    edtTotal: TEdit;
    btnIncluir: TButton;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Label9: TLabel;
    vlTotalPedido: TStaticText;
    btnFecharPedido: TButton;
    btnBucarPedido: TButton;
    btnCancelar: TButton;
    edtCodPedido: TEdit;
    procedure edtCodClienteExit(Sender: TObject);
    procedure edtCodProdExit(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnFecharPedidoClick(Sender: TObject);
    procedure btnBucarPedidoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    var
      codPedido: integer;
  public
    { Public declarations }
    procedure abrePedido;

  end;

var
  frmPedido: TfrmPedido;

implementation

uses
  uController.Cliente, uController.Interfaces, uModel.Cliente, uModel.Pedido,
  uController.Produto, uController.Pedido.Itens, uModel.Pedido.Itens,
  uController.Pedido;

{$R *.dfm}

procedure TfrmPedido.abrePedido;
var
  FControllerPedido : uControllerPedido<TModelPedido>;
begin
  FControllerPedido.This.Data_Emissao := Date;
  FControllerPedido.This.Valor_Total := 0;
  FControllerPedido.Insert;
  codPedido  := FControllerPedido.This.Codigo;

  edtCodPedido.Text := IntToStr(codPedido);
  edtCodCliente.SetFocus;
end;

procedure TfrmPedido.btnBucarPedidoClick(Sender: TObject);
var
  FControllerPedido  : uControllerPedido<TModelPedido>;
  FcontrollerPedidosItens : uControllerPedidoItem<TModelPedidoItens>;
begin
  FControllerPedido.This.Codigo := StrToInt( edtCodPedido.text);
  FControllerPedido.Get;
  FcontrollerPedidosItens.Get(StrToInt( edtCodPedido.text));
end;

procedure TfrmPedido.btnCancelarClick(Sender: TObject);
var
  FControllerPedido  : uControllerPedido<TModelPedido>;
  FcontrollerPedidosItens : uControllerPedidoItem<TModelPedidoItens>;
begin
  if MessageDlg('Deseja cancelar ?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
  begin
    FControllerPedido.This.Codigo := StrToInt( edtCodPedido.Text);
    FControllerPedido.Cancel;
    FcontrollerPedidosItens.This.Num_Pedido := FControllerPedido.This.Codigo;
    FcontrollerPedidosItens.Cancel;
  end;
end;

procedure TfrmPedido.btnFecharPedidoClick(Sender: TObject);
var
  FControllerPedido  : uControllerPedido<TModelPedido>;
  FcontrollerPedidosItens : uControllerPedidoItem<TModelPedidoItens>;
begin
  FControllerPedido.grava;
  FcontrollerPedidosItens.grava;
end;

procedure TfrmPedido.btnIncluirClick(Sender: TObject);
var
  FControllerPedidoItens  : TControllerPedidoItens;
begin
  FControllerPedidoItens.This.Cod_Produto := codPedido;
  FControllerPedidoItens.This.Cod_Produto := StrToInt( edtCodProd.Text );
  FControllerPedidoItens.This.Qtd := StrToCurr( edtQtd.Text);
  FControllerPedidoItens.This.Vl_Unitario := StrToCurr(edtVlUnitario.Text);
  FControllerPedidoItens.This.Vl_Total := StrToCurr(edtTotal.Text);

  FControllerPedidoItens.Insert;

  vlTotalPedido.Caption := CurrToStr( FControllerPedidoItens.TotalPedido(codPedido));

  edtCodProd.SetFocus;
end;

procedure TfrmPedido.edtCodClienteExit(Sender: TObject);
var
  FControlerCliente : uControllerGenerico<TModelCliente>;
begin
  FControlerCliente.GetId(StrToInt(edtCodCliente.text));
  edtNome.Text := dsCliente.DataSet.FieldByName('nome').AsString;
  edtCidade.Text  := dsCliente.DataSet.FieldByName('cidade').AsString;
  edtUF.Text  := dsCliente.DataSet.FieldByName('uf').AsString;

end;

procedure TfrmPedido.edtCodProdExit(Sender: TObject);
var
  FControllerProduto  : TControllerProduto;
begin
  FControllerProduto.GetId(StrToInt(edtCodProd.Text));
  edtNomeProd.Text := dsProduto.DataSet.FieldByName('descricao').AsString;
  edtVlUnitario.Text := dsProduto.DataSet.FieldByName('preco').AsString;
  edtQtd.Text := IntToStr(1);
  edtQtd.SetFocus;
end;

procedure TfrmPedido.FormShow(Sender: TObject);
begin
  abrePedido;
end;

end.
