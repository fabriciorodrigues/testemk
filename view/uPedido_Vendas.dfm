object frmPedido: TfrmPedido
  Left = 0
  Top = 0
  Caption = 'PDV'
  ClientHeight = 482
  ClientWidth = 927
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 921
    Height = 73
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = -77
    ExplicitTop = 44
    ExplicitWidth = 983
    object Label1: TLabel
      Left = 16
      Top = 17
      Width = 73
      Height = 17
      Caption = 'C'#243'd. Cliente:'
    end
    object Label2: TLabel
      Left = 127
      Top = 14
      Width = 82
      Height = 17
      Caption = 'Nome Cliente:'
    end
    object Label3: TLabel
      Left = 606
      Top = 14
      Width = 44
      Height = 17
      Caption = 'Cidade:'
    end
    object Label4: TLabel
      Left = 784
      Top = 14
      Width = 18
      Height = 17
      Caption = 'UF:'
    end
    object edtCodCliente: TEdit
      Left = 16
      Top = 34
      Width = 105
      Height = 25
      TabOrder = 0
      OnExit = edtCodClienteExit
    end
    object edtNome: TEdit
      Left = 127
      Top = 34
      Width = 473
      Height = 23
      TabStop = False
      Color = 15790320
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 1
    end
    object edtCidade: TEdit
      Left = 606
      Top = 34
      Width = 172
      Height = 23
      TabStop = False
      Color = 15790320
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 2
    end
    object edtUF: TEdit
      Left = 784
      Top = 34
      Width = 119
      Height = 23
      TabStop = False
      Color = 15790320
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 82
    Width = 921
    Height = 111
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label5: TLabel
      Left = 16
      Top = 18
      Width = 81
      Height = 17
      Caption = 'C'#243'd. Produto:'
    end
    object Label6: TLabel
      Left = 127
      Top = 18
      Width = 87
      Height = 17
      Caption = 'Nome Produto'
    end
    object Label7: TLabel
      Left = 606
      Top = 18
      Width = 71
      Height = 17
      Caption = 'Quantidade:'
    end
    object label20: TLabel
      Left = 687
      Top = 18
      Width = 83
      Height = 17
      Caption = 'Valor Unit'#225'rio:'
    end
    object Label8: TLabel
      Left = 784
      Top = 18
      Width = 31
      Height = 17
      Caption = 'Total:'
    end
    object edtCodProd: TEdit
      Left = 16
      Top = 37
      Width = 105
      Height = 25
      TabOrder = 0
      OnExit = edtCodProdExit
    end
    object edtNomeProd: TEdit
      Left = 127
      Top = 37
      Width = 473
      Height = 23
      Color = 15790320
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 1
    end
    object edtQtd: TEdit
      Left = 606
      Top = 37
      Width = 71
      Height = 25
      TabOrder = 2
    end
    object edtVlUnitario: TEdit
      Left = 687
      Top = 36
      Width = 91
      Height = 25
      TabOrder = 3
    end
    object edtTotal: TEdit
      Left = 784
      Top = 37
      Width = 119
      Height = 23
      Color = 15790320
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 4
    end
    object btnIncluir: TButton
      Left = 784
      Top = 69
      Width = 119
      Height = 31
      Caption = 'Incluir Produto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btnIncluirClick
    end
  end
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 8
    Top = 191
    Width = 595
    Height = 228
    DrawingStyle = gdsGradient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'cod_produto'
        Title.Caption = 'C'#243'd. Prod.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'descricao'
        Title.Caption = 'Nome Produto'
        Width = 400
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'quantidade'
        Title.Caption = 'QTD'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vl_unitario'
        Title.Caption = 'Vlr. Unit'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vl_total'
        Title.Caption = 'Vlr. Total'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 425
    Width = 927
    Height = 57
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -62
    ExplicitWidth = 989
    object Label9: TLabel
      Left = 516
      Top = 3
      Width = 84
      Height = 16
      Caption = 'Total Pedido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object vlTotalPedido: TStaticText
      Left = 528
      Top = 25
      Width = 59
      Height = 23
      Caption = '500,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 174
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object btnFecharPedido: TButton
    Left = 787
    Top = 199
    Width = 115
    Height = 40
    Caption = 'Fechar Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btnFecharPedidoClick
  end
  object btnBucarPedido: TButton
    Left = 641
    Top = 245
    Width = 115
    Height = 40
    Caption = 'Buscar Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btnBucarPedidoClick
  end
  object btnCancelar: TButton
    Left = 787
    Top = 245
    Width = 115
    Height = 40
    Caption = 'Cancelar Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnClick = btnCancelarClick
  end
  object edtCodPedido: TEdit
    Left = 641
    Top = 214
    Width = 115
    Height = 25
    TabOrder = 7
    OnExit = edtCodClienteExit
  end
  object dsCliente: TDataSource
    Left = 424
    Top = 2
  end
  object dsProduto: TDataSource
    Left = 480
    Top = 2
  end
  object dsProductOrder: TDataSource
    Left = 592
    Top = 2
  end
end
