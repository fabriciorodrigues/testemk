unit uModel.Conexao.Interfaces;

interface
  uses Data.DB;
  type
    uModelConnectionsG = interface
      ['{70870434-E935-4501-88F0-EAD99A7948CA}']
      function DataSet            :TDataSet;
      function TransacaoStart     :uModelConnectionsG;
      function TransacaoRollBack  :uModelConnectionsG;
      function TransacaoCommit    :uModelConnectionsG;
      function Close              :uModelConnectionsG;
      function Clear              :uModelConnectionsG;
      function Add(SQL:string)    :uModelConnectionsG;
      function ParamByName(aParam:String;aValue:Variant)  :uModelConnectionsG;
      function ExecSQL            :uModelConnectionsG;
      function Open               :uModelConnectionsG;
      function FielByName(aField:string):Variant;
    end;

    uModelConexao = interface
      ['{ACB40793-98BE-452A-BB79-9F704B449323}']
      function Connection : uModelConnectionsG;
    end;

implementation

end.
