unit uModel.Conexao;

interface

uses
  uModel.Conexao.Interfaces, uModel.Conexao.Firedac;
  type
  TModelConnections = class(TInterfacedObject, uModelConexao)
    private
    FConnection :uModelConnectionsG;
    public
    constructor Create;
    destructor Destroy; override;
    class function New  :uModelConexao;
    function Connection :uModelConnectionsG;
  end;

implementation



{ TModelConnections }

function TModelConnections.Connection: uModelConnectionsG;
begin
  if not Assigned(FConnection) then
    FConnection := TModelConexaoFiredac.New;

  Result := FConnection;
end;

constructor TModelConnections.Create;
begin

end;

destructor TModelConnections.Destroy;
begin

  inherited;
end;

class function TModelConnections.New: uModelConexao;
begin
  Result := Self.Create;
end;

end.
