unit uModel.Conexao.Firedac;

interface

uses
  Data.DB, uModel.Conexao.Interfaces, FireDAC.Comp.Client;
  type
    TModelConexaoFiredac = class(TInterfacedObject, uModelConnectionsG)
    private
      FDataSouce    :TDataSource;
      FQuery        :TFDQuery;
      FConnection   :TFDConnection;

    public
      constructor Create;
      Destructor Destroy;override;
      class function New : uModelConnectionsG;
      function DataSet            :TDataSet;
      function TransacaoStart     :uModelConnectionsG;
      function TransacaoRollBack  :uModelConnectionsG;
      function TransacaoCommit    :uModelConnectionsG;
      function Close              :uModelConnectionsG;
      function Clear              :uModelConnectionsG;
      function Add (SQL: string)  :uModelConnectionsG;
      function ParamByName(aParam:String;aValue:Variant)  :uModelConnectionsG;
      function ExecSQL            :uModelConnectionsG;
      function Open               :uModelConnectionsG;
      function FielByName(aField:string):Variant;
    end;

implementation

{ TModelConexaoFiredac }

function TModelConexaoFiredac.Add(SQL: string): uModelConnectionsG;
begin
  Result := Self;
  FQuery.SQL.Add(SQL);
end;

function TModelConexaoFiredac.Clear: uModelConnectionsG;
begin
  Result := Self;
  FQuery.SQL.Clear;
end;

function TModelConexaoFiredac.Close: uModelConnectionsG;
begin
  Result := Self;
  FQuery.Close;
end;

constructor TModelConexaoFiredac.Create;
begin
  FConnection := TFDConnection.Create(nil);
  FConnection.Params.DriverID := 'Ora';
  FConnection.Params.Database := 'admin';
  FConnection.Params.UserName := 'SYSTEM';
  FConnection.Params.Add('port=1521');
  FConnection.Params.Add('server=localhost');

  FQuery := TFDQuery.Create(nil);
  FQuery.Connection := FConnection;
end;

function TModelConexaoFiredac.DataSet: TDataSet;
begin
  Result := FQuery;
  FQuery.RecordCount;
end;

destructor TModelConexaoFiredac.Destroy;
begin
  FQuery.Free;
  FConnection.Free;
  inherited;
end;

function TModelConexaoFiredac.ExecSQL: uModelConnectionsG;
begin
  Result := Self;
  FQuery.ExecSQL;
end;


function TModelConexaoFiredac.FielByName(aField: string): Variant;
begin
  Result := FQuery.FieldByName(aField).Value;
end;

class function TModelConexaoFiredac.New: uModelConnectionsG;
begin
  Result := Self.Create;
end;

function TModelConexaoFiredac.Open: uModelConnectionsG;
begin
  Result := Self;
  FQuery.Open();
end;

function TModelConexaoFiredac.ParamByName(aParam:String;aValue:Variant): uModelConnectionsG;
begin
  Result := Self;
  FQuery.ParamByName(aParam).Value := aValue;
end;

function TModelConexaoFiredac.TransacaoCommit: uModelConnectionsG;
begin
  FConnection.Commit;
end;

function TModelConexaoFiredac.TransacaoRollBack: uModelConnectionsG;
begin
  FConnection.RollbackRetaining;
end;

function TModelConexaoFiredac.TransacaoStart: uModelConnectionsG;
begin
  FConnection.StartTransaction;
end;

end.
