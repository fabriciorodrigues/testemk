unit uController.Cliente;

interface

uses
  uController.Interfaces, uModel.Cliente, Data.DB, uModel.Conexao.Interfaces;
  type
    TControllerCliente = class(TInterfacedObject, uControllerGenerico<TModelCliente>)
    private
      FModelCLiente :TModelCliente;
      FDataSource   :TDataSource;
      FConnection   :uModelConnectionsG;
    public
    constructor Create;
    destructor Destroy;override;
    class function New : uControllerGenerico<TModelCliente>;
    function DataSource(aValue:TDataSource):uControllerGenerico<TModelCliente>;
//    function Insert             :uControllerGenerico<TModelCliente>;
//    function Update             :uControllerGenerico<TModelCliente>;
//    function Delete             :uControllerGenerico<TModelCliente>;
//    function GetAll             :uControllerGenerico<TModelCliente>;
    function GetId(aId:Integer) :uControllerGenerico<TModelCliente>;
    function This               :TModelCliente;
    end;

implementation

uses
  uModel.Conexao;

{ TControllerCliente }

constructor TControllerCliente.Create;
begin
  if not Assigned(FConnection) then
    FConnection := TModelConnections.New.Connection;

  FModelCLiente := TModelCliente.Create;
end;

function TControllerCliente.DataSource(
  aValue: TDataSource): uControllerGenerico<TModelCliente>;
begin
  Result := Self;
  FDataSource := aValue;
  FDataSource.DataSet := FConnection.DataSet;
end;

destructor TControllerCliente.Destroy;
begin
  FModelCLiente.Free;
  inherited;
end;

function TControllerCliente.GetId(
  aId: Integer): uControllerGenerico<TModelCliente>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('select * from clientes')
  .Add('WHERE codigo = :codigo')
  .ParamByName('codigo', aId)
  .Open;
end;

class function TControllerCliente.New: uControllerGenerico<TModelCliente>;
begin
  Result := Self.Create;
end;

function TControllerCliente.This: TModelCliente;
begin
  Result := FModelCLiente;
end;

end.
