unit uController.Pedido;

interface

uses
  uModel.Conexao, uController.Interfaces, uModel.Pedido, Data.DB, uModel.Conexao.Interfaces;

type
  TControllerPedido = class(TInterfacedObject, uControllerPedido<TModelPedido>)
  private
    FConnection   :uModelConnectionsG;
    FDataSource   :TDataSource;
    FModelPedido  :TModelPedido;
  public
    constructor Create;
    destructor Destroy; override;
    class function New : uControllerPedido<TModelPedido>;
    function DataSource(aValue: TDataSource):uControllerPedido<TModelPedido>;
    function Insert :uControllerPedido<TModelPedido>;
    function Cancel :uControllerPedido<TModelPedido>;
    function This   :TModelPedido;
    function Add: uControllerPedido<TModelPedido>;
    function grava: uControllerPedido<TModelPedido>;
    function get  :uControllerPedido<TModelPedido>;
  end;

implementation

{ TControllerPedido }

function TControllerPedido.Add: uControllerPedido<TModelPedido>;
begin
  Result := Self;
end;

function TControllerPedido.Cancel: uControllerPedido<TModelPedido>;
begin
  Result  := Self;
  FConnection.TransacaoStart;

  FConnection
  .Close
  .Clear
  .Add('delete from pedidos ')
  .Add('where Num_Pedido = :num_pedido')
  .ParamByName('num_pedido', FModelPedido.Num_Pedido)
  .ExecSQL;

  FConnection.TransacaoCommit;
end;

constructor TControllerPedido.Create;
begin
  if not Assigned(FConnection) then
    FConnection := TModelConnections.New.Connection;

  FModelPedido := TModelPedido.Create
end;

function TControllerPedido.DataSource(
  aValue: TDataSource): uControllerPedido<TModelPedido>;
begin
  Result := Self;
  FDataSource := aValue;
  FDataSource.DataSet := FConnection.DataSet;
end;

destructor TControllerPedido.Destroy;
begin
  FModelPedido.Free;
  inherited;
end;

function TControllerPedido.get: uControllerPedido<TModelPedido>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('SELECT p.*, c.nome FROM pedidos p')
  .Add('LEFT JOIN cliente c ON c.codigo = p.cod_cliente')
  .Add('WHERE num_pedido = :num_pedido')
  .ParamByName('num_pedido', FModelPedido.Num_Pedido)
  .Open;
end;

function TControllerPedido.grava: uControllerPedido<TModelPedido>;
begin
  Result := Self;
  FConnection.TransacaoCommit;
end;

function TControllerPedido.Insert: uControllerPedido<TModelPedido>;
begin
  Result := Self;
  FConnection.TransacaoStart;

  FConnection.Close
  .Clear
  .Add('insert into pedidos ( data_emissao, cod_cliente, valor_total' )
  .Add('values (:data, cod_cliente, :valor_total)')
  .ParamByName('data', FModelPedido.Data_Emissao)
  .ParamByName('cod_cliente', FModelPedido.Cod_Cliente)
  .ParamByName('valor_total', FModelPedido.Valor_Total)
  .ExecSQL;

  FConnection.TransacaoCommit;
end;

class function TControllerPedido.New: uControllerPedido<TModelPedido>;
begin
  Result := Self.Create;
end;

function TControllerPedido.This: TModelPedido;
begin
  Result := FModelPedido;
end;

end.
