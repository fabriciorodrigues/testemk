unit uController.Produto;

interface
uses
  uController.Interfaces, Data.DB, uModel.Conexao.Interfaces,
  uModel.Produto;
  type
    TControllerProduto = class(TInterfacedObject, uControllerGenerico<TModelProduto>)
    private
      FModelProdut :TModelProduto;
      FDataSource   :TDataSource;
      FConnection   :uModelConnectionsG;
    public
    constructor Create;
    destructor Destroy;override;
    class function New : uControllerGenerico<TModelProduto>;
    function DataSource(aValue:TDataSource):uControllerGenerico<TModelProduto>;
    function GetId(aId:Integer) :uControllerGenerico<TModelProduto>;
    function This               :TModelProduto;
    end;
implementation

uses
  uModel.Conexao;

{ TControllerProduto }

constructor TControllerProduto.Create;
begin
  if not Assigned(FConnection) then
    FConnection := TModelConnections.New.Connection;

  FModelProdut := TModelProduto.Create;
end;

function TControllerProduto.DataSource(
  aValue: TDataSource): uControllerGenerico<TModelProduto>;
begin
  Result := Self;
  FDataSource := aValue;
  FDataSource.DataSet := FConnection.DataSet;
end;

destructor TControllerProduto.Destroy;
begin
  FModelProdut.Free;
  inherited;
end;

function TControllerProduto.GetId(
  aId: Integer): uControllerGenerico<TModelProduto>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('select * from produtos')
  .Add('WHERE codigo = :codigo')
  .ParamByName('codigo', aId)
  .Open;
end;

class function TControllerProduto.New: uControllerGenerico<TModelProduto>;
begin
  Result := Self.Create;
end;

function TControllerProduto.This: TModelProduto;
begin
  Result := FModelProdut;
end;

end.
