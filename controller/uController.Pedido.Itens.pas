unit uController.Pedido.Itens;

interface

uses
  uController.Interfaces, uModel.Pedido.Itens, uModel.Conexao.Interfaces,
  Data.DB;
  type
    TControllerPedidoItens = class(TInterfacedObject, uControllerPedidoItem<TModelPedidoItens>)
    private
      FConnection         :uModelConnectionsG;
      FDataSource         :TDataSource;
      FModelPedidoItens   :TModelPedidoItens;
    public
    constructor Create;
    destructor Destroy;override;
    class function New : uControllerPedidoItem<TModelPedidoItens>;
    function DataSource(aValue: TDataSource):uControllerPedidoItem<TModelPedidoItens>;
    function Insert :uControllerPedidoItem<TModelPedidoItens>;
    function Cancel :uControllerPedidoItem<TModelPedidoItens>;
    function This   :TModelPedidoItens;
    function grava  :uControllerPedidoItem<TModelPedidoItens>;
    function Get(aId: Integer): uControllerPedidoItem<TModelPedidoItens>;
    function TotalPedido(num_pedido: Integer) : Currency;

  end;

implementation

uses
  uModel.Conexao;

{ TControllerPedidoItens }

function TControllerPedidoItens.Cancel: uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('delete from pedido_itens ')
  .Add('where num_peido = :num_pedido')
  .ParamByName('num_pedido', FModelPedidoItens.Num_Pedido)
  .ExecSQL;
end;

constructor TControllerPedidoItens.Create;
begin
  if not Assigned(FConnection) then
    FConnection := TModelConnections.New.Connection;

  FModelPedidoItens := TModelPedidoItens.Create;
end;

function TControllerPedidoItens.DataSource(
  aValue: TDataSource): uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self;
  FDataSource := aValue;
  FDataSource.DataSet := FConnection.DataSet;
end;

destructor TControllerPedidoItens.Destroy;
begin
  FModelPedidoItens.Free;
  inherited;
end;

function TControllerPedidoItens.Get(
  aId: Integer): uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('SELECT p.codigo,p.descricao,pe.qtd,')
  .Add('pe.vl_Unitario, pe.vl_Total FROM pedido_itens pe')
  .Add('LEFT JOIN produtos ON p.codigo = pe.cod_Produto')
  .Add('WHERE pe.num_Pedido  = :num_Pedido ')
  .ParamByName('num_Pedido ',aId)
  .Open;
end;

function TControllerPedidoItens.grava: uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self;
  FConnection.TransacaoCommit;
end;

function TControllerPedidoItens.Insert: uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self;
  FConnection
  .Close
  .Clear
  .Add('insert into pedido_itens (num_pedido, cod_produto, qtd, vl_unitario, vl_total)')
  .Add('VALUES(:num_pedido, :cod_produto, :qtd, :vl_unitario, :vl_total')
  .ParamByName('num_pedido', FModelPedidoItens.Num_Pedido)
  .ParamByName('cod_produto', FModelPedidoItens.Cod_Produto)
  .ParamByName('qtd', FModelPedidoItens.Qtd)
  .ParamByName('vl_unitario', FModelPedidoItens.Vl_Unitario)
  .ParamByName('vl_total', FModelPedidoItens.Vl_Total)
  .ExecSQL;
end;

class function TControllerPedidoItens.New: uControllerPedidoItem<TModelPedidoItens>;
begin
  Result := Self.Create;
end;

function TControllerPedidoItens.This: TModelPedidoItens;
begin
  Result := FModelPedidoItens;
end;

function TControllerPedidoItens.TotalPedido(num_pedido: Integer): Currency;
var
  total:Currency;
begin
  FConnection
  .Close
  .Clear
  .Add(' select sum(vl_Total) from pedido_itens ')
  .Add('where num_pedido = :num_pedido ')
  .ParamByName('num_pedido', FModelPedidoItens.Num_Pedido)
  .Open;

  total := FConnection.FielByName('vl_Total');

  Result := total;
end;

end.
