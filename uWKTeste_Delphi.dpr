program uWKTeste_Delphi;

uses
  Vcl.Forms,
  uPedido_Vendas in 'view\uPedido_Vendas.pas' {frmPedido},
  uModel.Conexao.Interfaces in 'dao\uModel.Conexao.Interfaces.pas',
  uModel.Cliente in 'model\uModel.Cliente.pas',
  uModel.Produto in 'model\uModel.Produto.pas',
  uModel.Pedido in 'model\uModel.Pedido.pas',
  uModel.Pedido.Itens in 'model\uModel.Pedido.Itens.pas',
  uModel.Conexao.Firedac in 'dao\uModel.Conexao.Firedac.pas',
  uModel.Conexao in 'dao\uModel.Conexao.pas',
  uController.Interfaces in 'controller\uController.Interfaces.pas',
  uController.Pedido in 'controller\uController.Pedido.pas',
  uController.Pedido.Itens in 'controller\uController.Pedido.Itens.pas',
  uController.Cliente in 'controller\uController.Cliente.pas',
  uController.Produto in 'controller\uController.Produto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPedido, frmPedido);
  Application.Run;
end.
